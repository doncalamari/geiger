package serial

import (
	"../archive"
	"github.com/tarm/serial"
	"log"
	"time"
)

var total uint64 = 0

func ReadData(device_name string, baud int, log_location string) {

	config := &serial.Config{Name: device_name, Baud: baud}
	serial, error := serial.OpenPort(config)
	if error != nil {
		log.Fatal("Error opening device "+device_name+": ", error)
	}
	defer serial.Close()

	buffer := make([]byte, 1)

	cpm_start := time.Now()
	current_cpm := 0
	for {
		_, err := serial.Read(buffer)
		if err != nil {
			log.Println("Error reading from device ", err)
			return
		}

		now := time.Now()
		if now.Sub(cpm_start).Seconds() > 60 {
			go archive.ArchiveResult(current_cpm, now, log_location)
			cpm_start = now
			current_cpm = 0
		} else {
			current_cpm = current_cpm + 1
		}

	}

}
