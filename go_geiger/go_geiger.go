package main

import (
	"./serial"
	"flag"
)

var (
	device_name  = flag.String("device", "/dev/ttyUSB0", "Serial device providing random numbers")
	log_location = flag.String("log_location", "./", "Location of log files")
)

func main() {
	flag.Parse()
	serial.ReadData(*device_name, 9600, *log_location)
}
