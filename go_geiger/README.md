Random Number Websocket Service
================================
Reads the counts from the Sparkfun Geiger counter. https://www.sparkfun.com/products/11345
Fopr this to work you have to replace the firmware on the device with the one in ../geiger_firmware.

Details
-------
You will have to 'go get' some packages for this to compile. Should be easy to figure out.

To compile for the Raspberry pi either load the go compiler on your device or on another machine, run
```
GOARCH=arm GOOS=linux go build
```
