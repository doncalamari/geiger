package archive

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

func ArchiveResult(value int, time_stamp time.Time, log_location string) {

	file, err := os.OpenFile(log_location+get_current_filename(time_stamp), os.O_RDWR|os.O_APPEND|os.O_CREATE, 0600)
	defer file.Close()

	if err != nil {
		log.Println(err)
	}

	file.WriteString(time_stamp.Format(time.RFC3339) + "," + strconv.FormatInt(int64(value), 10) + "\n")
}

func get_current_filename(time_stamp time.Time) string {
	year, month, day := time_stamp.Date()
	return strconv.Itoa(year) + "-" + fmt.Sprintf("%02d", int(month)) + "-" + fmt.Sprintf("%02d", day) + ".csv"
}
