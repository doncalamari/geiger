volatile bool led_flip_flag = false;

uint8_t ledPin = A5;

void interrupt_handler() {
  Serial.print(".");
  led_flip_flag = !led_flip_flag;
  digitalWrite(ledPin, led_flip_flag);
}

void setup() {
  Serial.begin(9600);

  pinMode(2, INPUT);
  attachInterrupt(0, interrupt_handler, FALLING);

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
}

void loop() {

}
