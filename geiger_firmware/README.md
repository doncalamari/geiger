Sparkfun Geiger Counter Firmware
================================
Prints a dot every time the tube from the Sparkfun Geiger counter registers a hit. Useful for calculating counts per minute (CPM). https://www.sparkfun.com/products/11345

Details
-------
Open the sketch in the arduino IDE. Set the board type to Arduino Pro or mini. Set the processor type to ATMega328 (3.3v 8Mhz)

Upload sketch, then attach to the serial port at 9600 baud.
